package com.dbel.idealo.domain.core.di

import com.dbel.idealo.domain.core.di.providers.PlayGameUseCaseProvider
import com.dbel.idealo.domain.usecases.PlayGameUseCases
import toothpick.config.Module

class DomainModule() : Module() {
    init {
        bind(PlayGameUseCases::class.java).toProvider(PlayGameUseCaseProvider::class.java)
    }
}