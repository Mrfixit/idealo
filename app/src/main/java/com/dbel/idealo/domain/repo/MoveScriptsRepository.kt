package com.dbel.idealo.domain.repo

import com.dbel.idealo.domain.models.MoveLo
import io.reactivex.Single

interface MoveScriptsRepository {
    fun getChessMoves(): Single<List<MoveLo>>
}