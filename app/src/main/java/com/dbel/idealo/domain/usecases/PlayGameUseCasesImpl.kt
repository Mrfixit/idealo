package com.dbel.idealo.domain.usecases

import com.dbel.idealo.domain.core.BaseUseCase
import com.dbel.idealo.domain.models.MoveLo
import com.dbel.idealo.domain.repo.MoveScriptsRepository
import com.dbel.idealo.utils.schedulers.SchedulerManager
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PlayGameUseCasesImpl @Inject constructor(
    schedulerManager: SchedulerManager,
    private val repo: MoveScriptsRepository
) : PlayGameUseCases, BaseUseCase(schedulerManager) {

    override fun play(): Flowable<MoveLo> {
        return Flowable.zip(
            Flowable.interval(0, 1, TimeUnit.SECONDS),
            repo.getChessMoves().toFlowable().flatMapIterable { it },
            BiFunction { _: Long, move: MoveLo -> move })
            .applySchedulers()
    }

    override fun playFrom(num: Int): Flowable<MoveLo> {
        return Flowable.zip(
            Flowable.interval(0, 1, TimeUnit.SECONDS),
            repo.getChessMoves()
                .toFlowable()
                .map { it.subList(num, it.size) }
                .flatMapIterable { it },
            BiFunction { _: Long, move: MoveLo -> move })
            .applySchedulers()
    }

}