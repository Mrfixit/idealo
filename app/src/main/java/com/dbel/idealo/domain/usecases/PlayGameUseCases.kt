package com.dbel.idealo.domain.usecases

import com.dbel.idealo.domain.models.MoveLo
import io.reactivex.Flowable

interface PlayGameUseCases {

    fun play(): Flowable<MoveLo>

    fun playFrom(num: Int): Flowable<MoveLo>

}