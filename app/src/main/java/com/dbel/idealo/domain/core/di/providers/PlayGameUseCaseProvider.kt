package com.dbel.idealo.domain.core.di.providers

import com.dbel.idealo.domain.repo.MoveScriptsRepository
import com.dbel.idealo.domain.usecases.PlayGameUseCases
import com.dbel.idealo.domain.usecases.PlayGameUseCasesImpl
import com.dbel.idealo.utils.schedulers.SchedulerManager
import javax.inject.Inject
import javax.inject.Provider

class PlayGameUseCaseProvider @Inject constructor(
    private val schedulerManager: SchedulerManager,
    private val repository: MoveScriptsRepository
) : Provider<PlayGameUseCases> {
    override fun get(): PlayGameUseCases {
        return PlayGameUseCasesImpl(schedulerManager, repository)
    }

}