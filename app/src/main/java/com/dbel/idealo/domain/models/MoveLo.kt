package com.dbel.idealo.domain.models

data class MoveLo(
    val playerId: Int,
    val positionX: Int,
    val positionY: Int
) : BaseLo() {
}