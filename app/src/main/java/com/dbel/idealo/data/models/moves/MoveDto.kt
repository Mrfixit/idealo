package com.dbel.idealo.data.models.moves

import com.dbel.idealo.data.models.BaseDto
import com.google.gson.annotations.SerializedName

data class MoveDto(
    @SerializedName("p")
    val playerId: Int,
    @SerializedName("x")
    val positionX: Int,
    @SerializedName("y")
    val positionY: Int
) : BaseDto() {
}