package com.dbel.idealo.data.core.di

import android.app.Application
import com.dbel.idealo.data.core.assets.AssetsFileReader
import com.dbel.idealo.data.core.di.providers.AssetsFileReaderProvider
import com.dbel.idealo.data.core.di.providers.MoveScriptsRepositoryProvider
import com.dbel.idealo.domain.repo.MoveScriptsRepository
import toothpick.config.Module

class DataModule(application: Application) : Module() {

    init {
        bind(AssetsFileReader::class.java).toProvider(AssetsFileReaderProvider::class.java)
            .providesSingletonInScope()
        bindRepos()
    }


    private fun bindRepos() {
        bind(MoveScriptsRepository::class.java).toProvider(MoveScriptsRepositoryProvider::class.java)
            .providesSingletonInScope()
    }

}
