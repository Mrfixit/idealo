package com.dbel.idealo.data.models.moves

import com.dbel.idealo.data.models.BaseDto
import com.google.gson.annotations.SerializedName

class MovesResponse(
    @SerializedName("moves")
    val moves: List<MoveDto>
) : BaseDto() {
}