package com.dbel.idealo.data.core.assets

import android.content.Context
import java.io.IOException

class AssetsFileReaderImpl(
    private val context: Context
) : AssetsFileReader {

    override fun readAsset(name: String): String {
        val fileString: String

        try {
            val inputStream = context.assets.open(name)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            fileString = String(buffer)
        } catch (ex: IOException) {
            return ""
        }

        return fileString

    }
}