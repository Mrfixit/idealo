package com.dbel.idealo.data.models.moves

import com.dbel.idealo.domain.models.MoveLo

fun MoveDto.toDomain(): MoveLo = MoveLo(this.playerId, this.positionX, this.positionY)

fun List<MoveDto>.toDomainList(): List<MoveLo> = this.map { it.toDomain() }