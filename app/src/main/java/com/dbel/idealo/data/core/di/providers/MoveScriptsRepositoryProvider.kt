package com.dbel.idealo.data.core.di.providers

import com.dbel.idealo.data.core.assets.AssetsFileReader
import com.dbel.idealo.data.repo.MoveScriptsRepositoryImpl
import com.dbel.idealo.data.sources.local.MovesScriptLocalDataSource
import com.dbel.idealo.domain.repo.MoveScriptsRepository
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Provider

class MoveScriptsRepositoryProvider @Inject constructor(
    private val assetsFileReader: AssetsFileReader,
    private val gson: Gson
) : Provider<MoveScriptsRepository> {
    override fun get(): MoveScriptsRepository {
        return MoveScriptsRepositoryImpl(
            MovesScriptLocalDataSource(assetsFileReader, gson)
        )
    }
}