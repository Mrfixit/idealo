package com.dbel.idealo.data.sources.headers

import com.dbel.idealo.data.models.moves.MoveDto
import io.reactivex.Single

interface MovesScriptDataSource {
    fun getChessMoves(): Single<List<MoveDto>>
}