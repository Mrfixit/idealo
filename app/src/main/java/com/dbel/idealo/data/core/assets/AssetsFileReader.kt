package com.dbel.idealo.data.core.assets

interface AssetsFileReader {
    fun readAsset(name: String): String
}