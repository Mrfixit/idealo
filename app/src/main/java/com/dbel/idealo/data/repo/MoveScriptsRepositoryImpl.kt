package com.dbel.idealo.data.repo

import com.dbel.idealo.data.models.moves.toDomainList
import com.dbel.idealo.data.sources.headers.MovesScriptDataSource
import com.dbel.idealo.domain.models.MoveLo
import com.dbel.idealo.domain.repo.MoveScriptsRepository
import io.reactivex.Single
import javax.inject.Inject

class MoveScriptsRepositoryImpl @Inject constructor(
    private val localDataSource: MovesScriptDataSource
) : MoveScriptsRepository {

    override fun getChessMoves(): Single<List<MoveLo>> {
        return localDataSource.getChessMoves()
            .map { it.toDomainList() }
    }

}