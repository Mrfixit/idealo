package com.dbel.idealo.data.sources.local

import com.dbel.idealo.data.core.assets.AssetsFileReader
import com.dbel.idealo.data.models.moves.MoveDto
import com.dbel.idealo.data.models.moves.MovesResponse
import com.dbel.idealo.data.sources.headers.MovesScriptDataSource
import com.google.gson.Gson
import io.reactivex.Single

class MovesScriptLocalDataSource(
    private val assetsFileReader: AssetsFileReader,
    private val gson: Gson
) : MovesScriptDataSource {
    override fun getChessMoves(): Single<List<MoveDto>> {
        return Single.fromCallable {
            val json = assetsFileReader.readAsset("play.json")
            return@fromCallable gson.fromJson(json, MovesResponse::class.java)
        }.map { it.moves }
    }
}