package com.dbel.idealo.data.core.di.providers

import android.content.Context
import com.dbel.idealo.data.core.assets.AssetsFileReader
import com.dbel.idealo.data.core.assets.AssetsFileReaderImpl
import javax.inject.Inject
import javax.inject.Provider

class AssetsFileReaderProvider @Inject constructor(
    private val context: Context
) : Provider<AssetsFileReader> {
    override fun get(): AssetsFileReader {
        return AssetsFileReaderImpl(context)
    }
}