package com.dbel.idealo.utils.app

import android.app.Application
import com.dbel.idealo.utils.di.Injection
import com.jakewharton.threetenabp.AndroidThreeTen

class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        Injection.init(this)

        setupTime()
    }


    private fun setupTime() {
        AndroidThreeTen.init(this)
    }


}