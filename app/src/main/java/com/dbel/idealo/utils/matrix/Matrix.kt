package com.dbel.idealo.utils.matrix

import android.util.Log

class Matrix<T>(val xSize: Int, val ySize: Int, val array: Array<Array<T>>) {

    operator fun get(x: Int, y: Int): T {
        return array[x][y]
    }

    operator fun set(x: Int, y: Int, t: T) {
        array[x][y] = t
    }

    inline fun forEach(operation: (T) -> Unit) {
        array.forEach { it.forEach { operation.invoke(it) } }
    }

    inline fun forEachIndexed(operation: (x: Int, y: Int, T) -> Unit) {
        array.forEachIndexed { x, p -> p.forEachIndexed { y, t -> operation.invoke(x, y, t) } }
    }

    fun reverseColumns() {
        for (i in array.indices) {
            var j = 0
            var k: Int = xSize - 1
            while (j < k) {
                val t = array[j][i]
                array[j][i] = array[k][i]
                array[k][i] = t
                j++
                k--
            }
        }
    }

    fun prettyPrint() {
        var printingString = ""
        for (i in array.indices) {
            for (j in array[i].indices) {
                printingString += ("[$i, $j] = ${array[i][j].toString()} ")
            }
            printingString += ("\n")
        }

        Log.d("Matrix", printingString)
    }

    companion object {

        inline operator fun <reified T> invoke() =
            Matrix(0, 0, Array(0) { emptyArray<T>() })

        inline operator fun <reified T> invoke(
            xWidth: Int,
            yWidth: Int,
            operator: (Int, Int) -> (T)
        ): Matrix<T> {
            val array = Array(xWidth) {
                val x = it
                Array(yWidth) { operator(x, it) }
            }
            return Matrix(xWidth, yWidth, array)
        }
    }

}