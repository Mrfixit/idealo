package com.dbel.idealo.utils.schedulers

import io.reactivex.Scheduler

interface SchedulerManager {
    fun getIoScheduler(): Scheduler

    fun getMainScheduler(): Scheduler
}