package com.dbel.idealo.utils.app

import android.app.Application
import android.content.Context
import com.dbel.idealo.utils.schedulers.SchedulerManager
import com.dbel.idealo.utils.schedulers.SchedulerManagerImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import toothpick.config.Module

//Application level Module(aka application scope)
class ApplicationModule(application: Application) : Module() {

    init {
        bind(Application::class.java).toInstance(application)
        bind(Context::class.java).toInstance(application)
        bind(SchedulerManager::class.java).toInstance(SchedulerManagerImpl())
        bind(Gson::class.java).toInstance(GsonBuilder().create())

    }

}