package com.dbel.idealo.presentation.chess.di

import com.dbel.idealo.presentation.chess.ui.activities.MainActivity
import toothpick.config.Module

class MainModule(activity: MainActivity) : Module() {
    init {
        bind(MainActivity::class.java).toInstance(activity)
    }
}