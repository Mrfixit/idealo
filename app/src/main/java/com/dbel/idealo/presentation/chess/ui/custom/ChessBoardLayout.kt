package com.dbel.idealo.presentation.chess.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.dbel.idealo.R
import com.dbel.idealo.presentation.chess.models.BoardRect
import com.dbel.idealo.presentation.chess.models.Player
import com.dbel.idealo.utils.matrix.Matrix
import com.dbel.idealo.utils.px

class ChessBoardLayout : View {

    private val brightPaint: Paint = Paint().apply {
        style = Paint.Style.FILL
        this.color = ContextCompat.getColor(context, R.color.chess_rect_bright)
    }

    private val darkPaint: Paint = Paint().apply {
        style = Paint.Style.FILL
        this.color = ContextCompat.getColor(context, R.color.chess_rect_dark)
    }

    private val playerPaint: Paint = Paint().apply {
        style = Paint.Style.FILL
    }

    private val textPaint: Paint = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        textSize = 16.px.toFloat()
        textAlign = Paint.Align.CENTER
        color = ContextCompat.getColor(context, R.color.chess_rect_bright)
        typeface = Typeface.DEFAULT_BOLD
    }

    private var xSize: Int =
        DEFAULT_SIZE

    private var ySize: Int =
        DEFAULT_SIZE

    private lateinit var chessBoard: Matrix<BoardRect>

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val typedArray = getContext().theme.obtainStyledAttributes(
            attrs,
            R.styleable.ChessBoardLayout,
            0, 0
        )

        try {
            xSize = typedArray.getInteger(
                R.styleable.ChessBoardLayout_boardSize,
                DEFAULT_SIZE
            )
            ySize = typedArray.getInteger(
                R.styleable.ChessBoardLayout_boardSize,
                DEFAULT_SIZE
            )

        } finally {
            typedArray.recycle()
        }

        chessBoard = Matrix(
            xSize,
            ySize
        ) { x, y -> BoardRect(0, 0) }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)

        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        val width = when (widthMode) {
            View.MeasureSpec.EXACTLY -> widthSize
            View.MeasureSpec.AT_MOST -> DEFAULT_TILE_SIZE * xSize
            View.MeasureSpec.UNSPECIFIED -> DEFAULT_TILE_SIZE * xSize
            else -> DEFAULT_TILE_SIZE * xSize
        }

        val height = when (heightMode) {
            View.MeasureSpec.EXACTLY -> heightSize
            View.MeasureSpec.AT_MOST -> DEFAULT_TILE_SIZE * ySize
            View.MeasureSpec.UNSPECIFIED -> DEFAULT_TILE_SIZE * ySize
            else -> DEFAULT_TILE_SIZE * ySize
        }

        setMeasuredDimension(width, height)
        calculateChessBoardMeasures(width, height - DEFAULT_TILE_SIZE)
    }

    override fun onDraw(canvas: Canvas) {
        chessBoard.forEachIndexed { x, y, rect ->
            drawChessRectOn(rect, canvas)
            drawPlayerIfPossible(rect, canvas)
        }
    }

    fun movePlayer(player: Player, x: Int, y: Int) {
        findPlayerPositionIfExists(player)?.let {
            chessBoard[it.first, it.second].player = null
        }
        chessBoard[x, y].player = player
        invalidate()
    }

    fun restore(lastMoves: HashMap<Player, Pair<Int, Int>>) {
        for ((player, value) in lastMoves) {
            chessBoard[value.first, value.second].player = player
        }
        invalidate()
    }

    private fun drawChessRectOn(rect: BoardRect, canvas: Canvas) {
        canvas.drawRect(
            rect.x.toFloat(),
            rect.y.toFloat(),
            (rect.x + DEFAULT_TILE_SIZE).toFloat(),
            (rect.y + DEFAULT_TILE_SIZE).toFloat(),
            if (rect.color != 0) darkPaint else brightPaint
        )

    }

    private fun drawPlayerIfPossible(rect: BoardRect, canvas: Canvas) {
        rect.player?.let { player ->

            when (player.color) {
                Player.Companion.PlayerColor.RED -> playerPaint.color =
                    ContextCompat.getColor(context, R.color.red)
                Player.Companion.PlayerColor.GREEN -> playerPaint.color =
                    ContextCompat.getColor(context, R.color.green)
                Player.Companion.PlayerColor.BLUE -> playerPaint.color =
                    ContextCompat.getColor(context, R.color.blue)
            }

            canvas.drawCircle(
                (rect.x + DEFAULT_TILE_SIZE / 2).toFloat(),
                (rect.y + DEFAULT_TILE_SIZE / 2).toFloat(),
                (DEFAULT_PLAYER_SIZE / 2).toFloat(),
                playerPaint
            )

            canvas.drawText(
                player.getShortName(),
                (rect.x + (rect.x + DEFAULT_TILE_SIZE)) / 2f,
                ((rect.y + DEFAULT_TILE_SIZE / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)),
                textPaint
            )
        }
    }

    private fun findPlayerPositionIfExists(player: Player): Pair<Int, Int>? {
        this.chessBoard.forEachIndexed { x, y, rect ->
            if (rect.player != null && rect.player == player) {
                return Pair(x, y)
            }
        }
        return null
    }

    private fun calculateChessBoardMeasures(width: Int, height: Int) {
        chessBoard.forEachIndexed { x, y, oldChessRect ->
            chessBoard[x, y] = BoardRect(
                x * DEFAULT_TILE_SIZE,
                // reverse height, cuz (2) Each square is represented by coordinates,
                // (0,0) is the bottom left corner of the board.
                height - (y * DEFAULT_TILE_SIZE),
                oldChessRect.player,
                (x + y) % 2
            )
        }
    }

    companion object {

        private const val DEFAULT_SIZE = 7
        private val DEFAULT_TILE_SIZE = 40.px
        private val DEFAULT_PLAYER_SIZE = 30.px

    }

}