package com.dbel.idealo.presentation.core.mvp

import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState

abstract class BaseViewState<V : BaseView> : RestorableViewState<V>