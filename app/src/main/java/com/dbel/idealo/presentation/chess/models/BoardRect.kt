package com.dbel.idealo.presentation.chess.models

import com.dbel.idealo.presentation.core.vo.BaseVo

data class BoardRect(
    val x: Int = 0,
    val y: Int = 0,
    var player: Player? = null,
    val color: Int = 0
) : BaseVo() {

    override fun toString(): String {
        return "x=$x, y=$y, $player)"
    }

}