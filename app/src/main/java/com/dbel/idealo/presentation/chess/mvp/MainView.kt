package com.dbel.idealo.presentation.chess.mvp

import com.dbel.idealo.presentation.chess.models.Player
import com.dbel.idealo.presentation.core.mvp.BaseView

interface MainView : BaseView {

    fun showPlayingFinish()

    fun showPlayState()

    fun showStopState()

    fun showPauseState()

    fun movePlayer(player: Player, toX: Int, toY: Int)

    fun restorePlay(movesPassed: Int)
    fun restorePause(movesPasses: Int)

    fun restoreChessBoard(toList: HashMap<Player, Pair<Int, Int>>)


}