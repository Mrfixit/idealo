package com.dbel.idealo.presentation.core.ui.navigation

import com.dbel.idealo.presentation.core.mvp.BasePresenter
import com.dbel.idealo.presentation.core.mvp.BaseView
import com.dbel.idealo.presentation.core.mvp.BaseViewState
import com.dbel.idealo.presentation.core.ui.BaseActivity
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseNavigationActivity<V : BaseView,
        P : BasePresenter<V>,
        VS : BaseViewState<V>> : BaseActivity<V, P, VS>(), BaseView {

    @Inject
    protected lateinit var navigationHolder: LocalCiceroneHolder

    override fun onResumeFragments() {
        getCicerone().navigatorHolder.setNavigator(getLocalNavigator())
        super.onResumeFragments()
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    fun getSimpleName(): String = this::class.java.simpleName

    private fun getCicerone(): Cicerone<Router> {
        return navigationHolder.getCicerone(getSimpleName())
    }

    abstract fun getContainerId(): Int

    protected abstract fun getLocalNavigator(): Navigator

}