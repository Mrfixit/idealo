package com.dbel.idealo.presentation.chess.mvp

import android.os.Bundle
import com.dbel.idealo.presentation.chess.di.MainScope
import com.dbel.idealo.presentation.chess.models.Player
import com.dbel.idealo.presentation.core.mvp.BaseViewState
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState
import javax.inject.Inject

@MainScope
class MainViewState @Inject constructor() : BaseViewState<MainView>() {

    private var movesPasses: Int = 0
    private var playerLastMoves: HashMap<Player, Pair<Int, Int>> = hashMapOf()
    private var state: State = State.STOP

    override fun saveInstanceState(out: Bundle) {
        out.putSerializable(KEY_STATE, state)
        out.putInt(KEY_MOVES_PASSED, movesPasses)
    }

    override fun apply(view: MainView?, retained: Boolean) {
        when (state) {
            State.PLAY -> {
                view?.restoreChessBoard(playerLastMoves)
                view?.restorePlay(movesPasses)
            }
            State.PAUSE -> {
                view?.restoreChessBoard(playerLastMoves)
                view?.restorePause(movesPasses)
            }
            State.STOP -> {
            }
        }
    }

    override fun restoreInstanceState(input: Bundle?): RestorableViewState<MainView> {
        input?.let {
            state = input.getSerializable(KEY_STATE) as State
            movesPasses = input.getInt(KEY_MOVES_PASSED)
        }
        return this
    }

    fun setPlayState() {
        this.state = State.PLAY
    }

    fun setStopState() {
        this.movesPasses = 0
        this.playerLastMoves = hashMapOf()
        this.state = State.STOP
    }

    fun setPauseState() {
        this.state = State.PAUSE
    }

    fun setMove(player: Player, toX: Int, toY: Int) {
        playerLastMoves[player] = Pair(toX, toY)
        this.movesPasses++
    }


    companion object {

        private const val KEY_STATE = "KEY_STATE"
        private const val KEY_MOVES_PASSED = "KEY_MOVES_PASSED"

        private enum class State {
            PLAY,
            PAUSE,
            STOP,
        }
    }
}