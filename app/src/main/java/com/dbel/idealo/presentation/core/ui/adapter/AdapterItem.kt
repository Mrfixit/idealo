package com.dbel.idealo.presentation.core.ui.adapter

import android.os.Parcelable
import java.util.*

abstract class AdapterItem(
    private val privateId: String = UUID.randomUUID().toString()
) : Parcelable