package com.dbel.idealo.presentation.chess.models

import com.dbel.idealo.domain.models.MoveLo

fun MoveLo.toViewObject(): Move = Move(
    Player(this.playerId, pickPlayerColorById(this.playerId)),
    this.positionX,
    this.positionY
)

private fun pickPlayerColorById(id: Int): Player.Companion.PlayerColor {
    return when (id) {
        0 -> Player.Companion.PlayerColor.RED
        1 -> Player.Companion.PlayerColor.GREEN
        2 -> Player.Companion.PlayerColor.BLUE
        else -> Player.Companion.PlayerColor.BLUE
    }
}