package com.dbel.idealo.presentation.core.mvp

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<V : BaseView> : MvpBasePresenter<V>() {

    private val subscriptionManager: CompositeDisposable = CompositeDisposable()

    protected fun add(disposable: Disposable) {
        subscriptionManager.add(disposable)
    }

    protected fun clearSubscriptions() {
        subscriptionManager.clear()
    }

    override fun detachView() {
        subscriptionManager.clear()
        super.detachView()
    }

    override fun destroy() {
        subscriptionManager.clear()
        super.destroy()
    }

}