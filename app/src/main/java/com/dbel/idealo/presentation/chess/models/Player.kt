package com.dbel.idealo.presentation.chess.models

import com.dbel.idealo.presentation.core.vo.BaseVo

data class Player(
    val id: Int,
    val color: PlayerColor
) : BaseVo() {

    fun getShortName(): String {
        return when (color) {
            PlayerColor.RED -> "R"
            PlayerColor.GREEN -> "G"
            PlayerColor.BLUE -> "B"
            else -> "U"
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Player

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }

    companion object {
        enum class PlayerColor {
            RED,
            GREEN,
            BLUE
        }
    }

}