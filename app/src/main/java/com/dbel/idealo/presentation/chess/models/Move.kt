package com.dbel.idealo.presentation.chess.models

import com.dbel.idealo.presentation.core.vo.BaseVo

data class Move(
    val player: Player,
    val positionX: Int,
    val positionY: Int
) : BaseVo() {
}