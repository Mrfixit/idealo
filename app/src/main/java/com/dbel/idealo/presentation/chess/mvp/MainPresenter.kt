package com.dbel.idealo.presentation.chess.mvp

import com.dbel.idealo.domain.usecases.PlayGameUseCases
import com.dbel.idealo.presentation.chess.di.MainScope
import com.dbel.idealo.presentation.chess.models.toViewObject
import com.dbel.idealo.presentation.core.mvp.BasePresenter
import javax.inject.Inject

@MainScope
class MainPresenter @Inject constructor(
    private val playGameUseCases: PlayGameUseCases
) : BasePresenter<MainView>() {

    private var currentPlayingPosition: Int = 0

    fun onInit() {
        ifViewAttached { view -> view.showStopState() }
    }

    fun onRestorePlay(movesPassed: Int) {
        this.currentPlayingPosition = movesPassed
        startPlayingFromPosition(movesPassed)
    }

    fun onRestorePause(movesPasses: Int) {
        this.currentPlayingPosition = movesPasses
    }

    fun onPlayButtonClicked(isPlaying: Boolean) {
        if (!isPlaying) {
            if (currentPlayingPosition == 0) {
                startPlaying()
            } else {
                startPlayingFromPosition(currentPlayingPosition)
            }
        } else {
            pausePlaying()
        }
    }


    private fun startPlaying() {
        this.add(
            playGameUseCases.play()
                .doOnSubscribe { ifViewAttached { view -> view.showPlayState() } }
                .doOnComplete {
                    ifViewAttached { view -> view.showStopState() }
                    this.currentPlayingPosition = 0
                }
                .doOnNext { this.currentPlayingPosition++ }
                .map { it.toViewObject() }
                .subscribe({
                    ifViewAttached { view ->
                        view.movePlayer(
                            it.player,
                            it.positionX,
                            it.positionY
                        )
                    }
                }, {
                    ifViewAttached { view -> view.showError(it.localizedMessage) }
                }, {
                    ifViewAttached { view -> view.showPlayingFinish() }
                })
        )
    }

    private fun startPlayingFromPosition(position: Int) {
        this.add(
            playGameUseCases.playFrom(position)
                .doOnSubscribe { ifViewAttached { view -> view.showPlayState() } }
                .doOnComplete {
                    ifViewAttached { view -> view.showStopState() }
                    this.currentPlayingPosition = 0
                }
                .doOnNext { this.currentPlayingPosition++ }
                .map { it.toViewObject() }
                .subscribe({
                    ifViewAttached { view ->
                        view.movePlayer(
                            it.player,
                            it.positionX,
                            it.positionY
                        )
                    }
                }, {
                    ifViewAttached { view -> view.showError(it.localizedMessage) }
                }, {
                    ifViewAttached { view -> view.showPlayingFinish() }
                })
        )
    }

    private fun pausePlaying() {
        clearSubscriptions()
        ifViewAttached { view -> view.showPauseState() }
    }


}