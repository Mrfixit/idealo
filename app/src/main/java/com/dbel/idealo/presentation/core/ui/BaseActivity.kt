package com.dbel.idealo.presentation.core.ui

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.dbel.idealo.R
import com.dbel.idealo.presentation.core.mvp.BasePresenter
import com.dbel.idealo.presentation.core.mvp.BaseView
import com.dbel.idealo.presentation.core.mvp.BaseViewState
import com.dbel.idealo.utils.di.Injection
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateActivity
import toothpick.Scope
import toothpick.Toothpick
import toothpick.config.Module

abstract class BaseActivity<V : BaseView,
        P : BasePresenter<V>,
        VS : BaseViewState<V>> : MvpViewStateActivity<V, P, VS>(),
    BaseView, OnBaseCallbackListener {

    private lateinit var component: Scope

    protected var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        component = setupComponent()
        Toothpick.inject(this, component)
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        getPresenter().attachView(mvpView)

    }

    override fun onDestroy() {
        hideLoading()
        getPresenter().detachView()
        Toothpick.closeScope(getDeclaredScope())
        super.onDestroy()
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }

        dialog = AlertDialog.Builder(this)
            .setTitle("")
            .setCancelable(false)
            .setPositiveButton("") { _, _ -> }
            .setOnDismissListener { dialog?.show() }
            .setMessage(getString(R.string.loading_text))
            .show()
    }

    override fun hideLoading() {
        dialog?.let {
            it.dismiss()
            dialog = null
        }
    }


    protected fun setupComponent(): Scope {
        val component = Toothpick.openScopes(Injection.ROOT_SCOPE, getDeclaredScope())
        component.installModules(*getDeclaredModules())
        return component
    }


    ///////////////////////////////////////////////////////////////////////////
    // ABSTRACT DI
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Setup {@code Module}s for injection. They could use {@code PresenterScope}.
     *
     * @return Array of needed {@code Module}
     */
    protected abstract fun getDeclaredModules(): Array<Module>

    /**
     * Get component Class of the screen that should be controlled by this BaseMvpActivity.
     *
     * @return Class of component
     */
    abstract fun getDeclaredScope(): Class<*>

    ///////////////////////////////////////////////////////////////////////////
    // ABSTRACT UI
    ///////////////////////////////////////////////////////////////////////////

    @LayoutRes
    protected abstract fun getLayout(): Int
}