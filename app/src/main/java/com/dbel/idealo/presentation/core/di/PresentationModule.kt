package com.dbel.idealo.presentation.core.di

import android.app.Application
import com.dbel.idealo.presentation.core.ui.navigation.LocalCiceroneHolder
import com.dbel.idealo.presentation.core.ui.navigation.di.NavigationHolderProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import toothpick.config.Module

class PresentationModule(application: Application) : Module() {

    init {
        bind(Cicerone::class.java).toInstance(Cicerone.create())
        bind(NavigatorHolder::class.java)
            .toProvider(NavigationHolderProvider::class.java)
            .providesSingletonInScope()
        bind(LocalCiceroneHolder::class.java).toInstance(LocalCiceroneHolder())
    }

}