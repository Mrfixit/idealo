package com.dbel.idealo.presentation.chess.ui.activities

import android.os.Bundle
import android.widget.Toast
import com.dbel.idealo.R
import com.dbel.idealo.presentation.chess.di.MainModule
import com.dbel.idealo.presentation.chess.di.MainScope
import com.dbel.idealo.presentation.chess.models.Player
import com.dbel.idealo.presentation.chess.mvp.MainPresenter
import com.dbel.idealo.presentation.chess.mvp.MainView
import com.dbel.idealo.presentation.chess.mvp.MainViewState
import com.dbel.idealo.presentation.core.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import toothpick.config.Module
import javax.inject.Inject

class MainActivity : BaseActivity<
        MainView,
        MainPresenter,
        MainViewState>(),
    MainView {

    @Inject
    protected lateinit var injectedPresenter: MainPresenter

    @Inject
    protected lateinit var injectedViewState: MainViewState

    override fun getDeclaredModules(): Array<Module> = arrayOf(MainModule(this))

    override fun getDeclaredScope(): Class<*> = MainScope::class.java

    override fun getLayout(): Int = R.layout.activity_main

    override fun onNewViewStateInstance() {
        getPresenter().onInit()
    }

    override fun createPresenter(): MainPresenter = injectedPresenter

    override fun createViewState(): MainViewState = injectedViewState

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUi()
    }

    override fun showPlayState() {
        button_play.isSelected = true
        getViewState()?.setPlayState()
    }

    override fun showPauseState() {
        button_play.isSelected = false
        getViewState()?.setPauseState()
    }

    override fun showStopState() {
        button_play.isSelected = false
        getViewState()?.setStopState()
    }

    override fun movePlayer(player: Player, toX: Int, toY: Int) {
        view_chessboard.movePlayer(player, toX, toY)
        getViewState()?.setMove(player, toX, toY)
    }

    override fun restorePlay(movesPassed: Int) {
        getPresenter().onRestorePlay(movesPassed)
    }

    override fun restorePause(movesPasses: Int) {
        getPresenter().onRestorePause(movesPasses)
    }
    override fun restoreChessBoard(lastMoves: HashMap<Player, Pair<Int, Int>>) {
        view_chessboard.restore(lastMoves)
    }

    override fun showPlayingFinish() {
        Toast.makeText(this, R.string.end, Toast.LENGTH_LONG).show()
    }

    private fun initUi() {
        button_play.setOnClickListener { getPresenter().onPlayButtonClicked(it.isSelected) }
    }

}
