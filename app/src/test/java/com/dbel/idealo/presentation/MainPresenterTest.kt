package com.dbel.idealo.presentation

import com.dbel.idealo.domain.models.MoveLo
import com.dbel.idealo.domain.usecases.PlayGameUseCases
import com.dbel.idealo.presentation.chess.mvp.MainPresenter
import com.dbel.idealo.presentation.chess.mvp.MainView
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MainPresenterTest {

    @Mock
    lateinit var view: MainView

    @Mock
    lateinit var useCase: PlayGameUseCases

    private lateinit var presenter: MainPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(useCase)
        presenter.attachView(view)
    }

    @Test
    fun mainPresenter_firstInit_should_show_default_state() {
        presenter.onInit()
        verify(view, times(1)).showStopState()
    }

    @Test
    fun mainPresenter_onRestorePlay_should_start_playing_fromPosition() {

        val movesPassed = 5

        whenever(useCase.playFrom(any())).thenReturn(
            Flowable.just(
                MoveLo(0, 0, 0),
                MoveLo(1, 2, 3),
                MoveLo(2, 3, 4)
            )
        )

        presenter.onRestorePlay(movesPassed)

        verify(useCase, times(1)).playFrom(movesPassed)
    }

    @Test
    fun mainPresenter_onPlayButtonClicked_firstTime_should_start_playing() {
        presenter.attachView(view)

        val isPlaying = false

        whenever(useCase.play()).thenReturn(
            Flowable.just(
                MoveLo(0, 0, 0),
                MoveLo(1, 2, 3),
                MoveLo(2, 3, 4)
            )
        )

        presenter.onPlayButtonClicked(isPlaying)

        verify(useCase, times(1)).play()
    }
}