package com.dbel.idealo.domain

import com.dbel.idealo.domain.repo.MoveScriptsRepository
import com.dbel.idealo.domain.usecases.PlayGameUseCases
import com.dbel.idealo.domain.usecases.PlayGameUseCasesImpl
import com.dbel.idealo.utils.schedulers.SchedulerManager
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PlayGameUseCaseTest {

    @Mock
    lateinit var schedulerManager: SchedulerManager

    @Mock
    lateinit var repository: MoveScriptsRepository

    private lateinit var useCase: PlayGameUseCases

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        whenever(schedulerManager.getIoScheduler()).thenReturn(Schedulers.trampoline())
        whenever(schedulerManager.getMainScheduler()).thenReturn(Schedulers.trampoline())

        useCase = PlayGameUseCasesImpl(schedulerManager, repository)
    }

    @Test
    fun useCase_call_play_should_call_repository() {
        whenever(repository.getChessMoves()).thenReturn(Single.just(arrayListOf()))

        val testObserver = useCase.play().test()

        verify(repository, times(1)).getChessMoves()

        println("useCase_call_play_should_call_repository PASSED")
    }

    @Test
    fun useCase_call_playFrom_should_call_repository() {
        whenever(repository.getChessMoves()).thenReturn(Single.just(arrayListOf()))

        val testObserver = useCase.playFrom(0).test()

        verify(repository, times(1)).getChessMoves()

        println("useCase_call_playFrom_should_call_repository PASSED")
    }

    // and so on like check the time to emit item with RxJavaPlugins && TestScheduler


}